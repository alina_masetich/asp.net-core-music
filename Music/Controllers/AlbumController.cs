﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Music.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Music.Controllers
{
    public class AlbumController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;


        public AlbumController(ApplicationDbContext context, IHostingEnvironment IHostingEnvironment)
        {
            _context = context;
            _environment = IHostingEnvironment;
        }

        public IActionResult Create()
        {
            ViewBag.Genres = new SelectList(_context.Genres, "Id", "Name");
            ViewBag.Artists = new SelectList(_context.Artists, "Id", "Name");
            return View();
        }

        [HttpPost]
        public IActionResult Create(string name, Album album)
        {
            var newFileName = string.Empty;

            if (HttpContext.Request.Form.Files != null)
            {
                var fileName = string.Empty;
                string PathDB = string.Empty;

                var files = HttpContext.Request.Form.Files;

                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        //Getting FileName
                        fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());

                        //Getting file Extension
                        var FileExtension = Path.GetExtension(fileName);

                        // concating  FileName + FileExtension
                        newFileName = myUniqueFileName + FileExtension;

                        // Combines two strings into a path.
                        fileName = Path.Combine(_environment.WebRootPath, "demoImages") + $@"\{newFileName}";

                        // if you want to store path of folder in database
                        PathDB = "demoImages/" + newFileName;

                        using (FileStream fs = System.IO.File.Create(fileName))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }


            }

            if (ModelState.IsValid)
            {
                album.AlbumArtUrl = newFileName;
                _context.Albums.Add(album);
                _context.SaveChanges();

                return RedirectToAction("Index", "Manager");
            }
            return View(album);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id != null)
            {
                Artist artist = await _context.Artists.FirstOrDefaultAsync(p => p.Id == id);
                if (artist != null)
                    return View(artist);
            }
            return NotFound();


        }
        [HttpPost]
        public async Task<IActionResult> Edit(Album album)
        {
            _context.Update(album);
            //_context.Entry(artist).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Manager");


        }

        [HttpGet]
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? id)
        {
            if (id != null)
            {
                Album album = await _context.Albums.FirstOrDefaultAsync(p => p.Id == id);
                if (album != null)
                    return View(album);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? id)
        {

            if (id != null)
            {
                Album album = new Album { Id = id.Value };
                _context.Entry(album).State = EntityState.Deleted;
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Manager");
            }
            return NotFound();
        }   

        public IActionResult GetEmpList()
        {
            var albums = _context.Albums.ToList();
            return View(albums);
        }

    }
}
