﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Music.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Music.Controllers
{
    public class ArtistController : Controller
    {
        private readonly ApplicationDbContext _context;
        
        public ArtistController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Artist artist)
        {
            if (ModelState.IsValid)
            {
                _context.Artists.Add(artist);
                _context.SaveChanges();
                return RedirectToAction("Index", "Manager");
            }
            return View(artist);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id != null)
            {
                Artist artist = await _context.Artists.FirstOrDefaultAsync(p => p.Id == id);
                if(artist != null)
                    return View(artist);
            }
            return NotFound();
            
            
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Artist artist)
        {   
            _context.Update(artist);
            //_context.Entry(artist).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Manager");
           
            
        }

        [HttpGet]
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? id)
        {
            if (id != null)
            {
                Artist artist = await _context.Artists.FirstOrDefaultAsync(p => p.Id == id);
                if (artist != null)
                    return View(artist);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? id)
        {
            
            if (id != null)
            {
                Artist artist = new Artist { Id = id.Value };
                _context.Entry(artist).State = EntityState.Deleted;
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Manager");
            }
            return NotFound();
        }

    }
}
