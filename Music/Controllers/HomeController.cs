﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Music.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using Music.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Music.Hubs;
using Microsoft.AspNetCore.Authorization;

namespace Music.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly IStringLocalizer<SharedResource> _sharedLocalizer;
        private UserManager<IdentityUser> _userManager;       
        private readonly IHubContext<CommentHub> _hubContext;
        public HomeController(IStringLocalizer<HomeController> localizer,
                   IStringLocalizer<SharedResource> sharedLocalizer, ApplicationDbContext context, UserManager<IdentityUser> userManager, IHubContext<CommentHub> hubContext)
        {
            _localizer = localizer;
            _sharedLocalizer = sharedLocalizer;
            _context = context;
            _userManager = userManager;
            _hubContext = hubContext;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllAlbums()
        {
            ViewBag.Genres = _context.Genres;
            var albums = _context.Albums;
            return View(albums);
        }

        public ActionResult Start()
        {
            ViewBag.Artists = _context.Artists;
            ViewBag.Genres = _context.Genres;
            ViewBag.Albums = _context.Albums;
            var genres = _context.Genres;
            return View(genres);
        }
        
        //Home/Browse?genre=Disco
        public ActionResult Browse(string genre)
        {
            ViewBag.Genre = genre;
            var genreModel = _context.Genres.Include("Albums")
                 .Single(g => g.Name == genre);
            return View(genreModel);
            
        }

        //Home/Details/5
     
        public ActionResult Details(int id)
        {
            var album = _context.Albums
                .Include(p => p.Songs)
                .Include(p => p.Comments)
                .ThenInclude(c => c.User)
                .FirstOrDefault(p => p.Id == id);
           
            return View(album);
            
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(int albumId, string content)
        {
            if (content == null)
            {
                return RedirectToAction("Details", new { id = albumId });
            }
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var album = _context.Albums.Include("Songs").
                Include("Comments").
                FirstOrDefault(p => p.Id == albumId);

            Comment comment = new Comment()
            {
                User = user,
                Content = content
            };
            album.Comments.Add(comment);
            _context.SaveChanges();
            await _hubContext.Clients.All.SendAsync("ReceiveComment", user.UserName, content, albumId);
            return RedirectToAction("Details", new { id = albumId });

        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
        
    }
}
