﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Music.Models;
using Microsoft.EntityFrameworkCore;
using Music.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Music.Controllers
{
    public class ManagerController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;

        
        public ManagerController(ApplicationDbContext context, IHostingEnvironment IHostingEnvironment)
        {
            _context = context;
            _environment = IHostingEnvironment;
        }
        
        public IActionResult Index()
        {
            
            ViewBag.Artists = _context.Artists;
            ViewBag.Genres = _context.Genres;
            ViewBag.Albums = _context.Albums;
            ViewBag.Songs = _context.Songs;
            return View();
        }

        //public IActionResult AddArtist()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public IActionResult AddArtist(Artist artist)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Artists.Add(artist);
        //        _context.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(artist);
        //}

        //public IActionResult AddAlbum()
        //{
        //    ViewBag.Genres = new SelectList(_context.Genres, "Id", "Name");
        //    ViewBag.Artists = new SelectList(_context.Artists, "Id", "Name");
        //    return View();
        //}

        //[HttpPost]
        //public IActionResult AddAlbum(string name, Album album)
        //{
        //    var newFileName = string.Empty;

        //    if (HttpContext.Request.Form.Files != null)
        //    {
        //        var fileName = string.Empty;
        //        string PathDB = string.Empty;

        //        var files = HttpContext.Request.Form.Files;

        //        foreach (var file in files)
        //        {
        //            if (file.Length > 0)
        //            {
        //                //Getting FileName
        //                fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

        //                //Assigning Unique Filename (Guid)
        //                var myUniqueFileName = Convert.ToString(Guid.NewGuid());

        //                //Getting file Extension
        //                var FileExtension = Path.GetExtension(fileName);

        //                // concating  FileName + FileExtension
        //                newFileName = myUniqueFileName + FileExtension;

        //                // Combines two strings into a path.
        //                fileName = Path.Combine(_environment.WebRootPath, "demoImages") + $@"\{newFileName}";

        //                // if you want to store path of folder in database
        //                PathDB = "demoImages/" + newFileName;

        //                using (FileStream fs = System.IO.File.Create(fileName))
        //                {
        //                    file.CopyTo(fs);
        //                    fs.Flush();
        //                }
        //            }
        //        }


        //    }

        //    if (ModelState.IsValid)
        //    {
        //        album.AlbumArtUrl = newFileName;
        //        _context.Albums.Add(album);
        //        _context.SaveChanges();

        //        return RedirectToAction("Index");
        //    }
        //    return View(album);
        //}

        //public IActionResult AddSong()
        //{
        //    ViewBag.Albums = new SelectList(_context.Albums, "Id", "Title");
        //    return View();
        //}

        //[HttpPost]
        //public IActionResult AddSong(Song song)
        //{
            
            
        //    if (ModelState.IsValid)
        //    {
                
        //        _context.Songs.Add(song);
        //        _context.SaveChanges();

        //        return RedirectToAction("Index");
        //    }
        //    return View(song);
        //}
    }
}
