﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Music.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Music.Controllers
{
    public class SongController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;

        public SongController(ApplicationDbContext context, IHostingEnvironment IHostingEnvironment)
        {
            _context = context;
            _environment = IHostingEnvironment;
        }

        public IActionResult Create()
        {
            ViewBag.Albums = new SelectList(_context.Albums, "Id", "Title");
            return View();
        }

        [HttpPost]
        public IActionResult Create(string name, Song song)
        {
            var newFileName = string.Empty;

            if (HttpContext.Request.Form.Files != null)
            {
                var fileName = string.Empty;
                string PathDB = string.Empty;

                var files = HttpContext.Request.Form.Files;

                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        //Getting FileName
                        fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());

                        //Getting file Extension
                        var FileExtension = Path.GetExtension(fileName);

                        // concating  FileName + FileExtension
                        newFileName = myUniqueFileName + FileExtension;

                        // Combines two strings into a path.
                        fileName = Path.Combine(_environment.WebRootPath, "audio") + $@"\{newFileName}";

                        // if you want to store path of folder in database
                        PathDB = "audio/" + newFileName;

                        using (FileStream fs = System.IO.File.Create(fileName))
                        {
                            file.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }


            }
            if (ModelState.IsValid)
            {
                song.AudiotUrl = newFileName;
                _context.Songs.Add(song);
                _context.SaveChanges();

                return RedirectToAction("Index", "Manager");
            }
            return View(song);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id != null)
            {
                Artist artist = await _context.Artists.FirstOrDefaultAsync(p => p.Id == id);
                if (artist != null)
                    return View(artist);
            }
            return NotFound();


        }
        [HttpPost]
        public async Task<IActionResult> Edit(Song song)
        {
            _context.Update(song);
            //_context.Entry(artist).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Manager");


        }

        [HttpGet]
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? id)
        {
            if (id != null)
            {
                Song song = await _context.Songs.FirstOrDefaultAsync(p => p.Id == id);
                if (song != null)
                    return View(song);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? id)
        {

            if (id != null)
            {
                Song song = new Song { Id = id.Value };
                _context.Entry(song).State = EntityState.Deleted;
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Manager");
            }
            return NotFound();
        }
    }
}
