﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Music.Hubs
{
    public class CommentHub : Hub
    {
        public async Task SendMessage(string user, string content, int albumId)
        {
            await Clients.All.SendAsync("ReceiveComment", user, content, albumId);
        }
    }
}
