﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Music.Models
{
    public class Album
    {
        [BindNever]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public decimal Price { get; set; } 
        public string AlbumArtUrl { get; set; }
        [Required]
        public int GenreId { get; set; }
        public Genre Genre { get; set; }
        [Required]
        public int ArtistId { get; set; }
        public Artist Artist { get; set; }

        public List<Song> Songs { get; set; }
        public List<Comment> Comments { get; } = new List<Comment>();
    }
}
