﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace Music.Models
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        {
           // Database.EnsureCreated();
       }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Song> Songs { get; set; }
        public DbSet<Comment> Comments { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>()
                .HasMany(p => p.Albums)
                .WithOne(p => p.Genre);

            modelBuilder.Entity<Album>()
               .HasOne(p => p.Artist)
               .WithMany(p => p.Albums);

            modelBuilder.Entity<Song>()
                .HasOne(p => p.Album)
                .WithMany(p => p.Songs);

            modelBuilder.Entity<Album>()
             .HasMany<Comment>(p => p.Comments)
             .WithOne();

            base.OnModelCreating(modelBuilder);

            
        }

    }
}
