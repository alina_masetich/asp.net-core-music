﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Music.Models
{
    public class Artist
    {
        [BindNever]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public List<Album> Albums { get; set; }
    }
}
