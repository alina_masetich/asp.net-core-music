﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Music.Models
{
    public class Comment
    {
        public int CommentId { get; set; }

        [Required]
        public IdentityUser User { get; set; }

        public virtual Album Album { get; set; }

        [Required]
        public string Content { get; set; }
    }
}
