﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Music.Models
{
    public static class DbInitializer
    {
        public static void Seed(ApplicationDbContext context)
        {

            if (!context.Genres.Any())
            {
                context.AddRange
                    (
                        new Genre { Name = "Rock" },
                        new Genre { Name = "Jazz" },
                        new Genre { Name = "Metal" },
                        new Genre { Name = "Alternative" },
                        new Genre { Name = "Disco" },
                        new Genre { Name = "Blues" },
                        new Genre { Name = "Latin" },
                        new Genre { Name = "Reggae" },
                        new Genre { Name = "Pop" },
                        new Genre { Name = "Classical" }
                    );
            }
            //foreach (var a in context.Artists)
            //{
            //    context.Artists.Remove(a);
            //}
            //context.SaveChanges();
            if (!context.Artists.Any())
            {
                context.AddRange
                    (
                        new Artist { Name = "Men At Work" },
                        new Artist { Name = "Aaron Copland & London Symphony Orchestra" },
                        new Artist { Name = "Aaron Goldberg" },
                        new Artist { Name = "AC/DC" },
                        new Artist { Name = "Accept" },
                        new Artist { Name = "Imagine Dragons"}
                    );
            }

            //foreach (var a in context.Albums)
            //{
            //    context.Albums.Remove(a);
            //}
            //context.SaveChanges();
            if (!context.Albums.Any())
            {
                context.AddRange
                    (
                        new Album
                        {
                            Title = "Evolve",
                            Genre = context.Genres.Single(g => g.Name == "Rock"),
                            Price = 8.99M,
                            Artist = context.Artists.Single(a => a.Name == "Imagine Dragons"),
                            AlbumArtUrl = "evolve.jpg"
                            
                        }
                    );
            }
            //foreach (var a in context.Songs)
            //{
            //    context.Songs.Remove(a);
            //}
            //context.SaveChanges();
            if (!context.Songs.Any())
            {
                context.AddRange
                    (
                        new Song
                        {
                            Name = "Beliver",
                            Album = context.Albums.Single(a => a.Title == "Evolve"),
                            AudiotUrl = "imagine-dragons-believer.mp3"
                        },
                        new Song
                        {
                            Name = "Thunder",
                            Album = context.Albums.Single(a => a.Title == "Evolve"),
                            AudiotUrl = "imagine-dragons-thunder.mp3"
                        }
                    );
            }
          
            context.SaveChanges();
            
        }
        
    }
    
}
