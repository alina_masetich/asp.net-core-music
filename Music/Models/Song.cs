﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Music.Models
{
    public class Song
    {
        [BindNever]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string AudiotUrl { get; set; }
        [Required]
        public int AlbumId { get; set; }
        public Album Album { get; set; }
    }
}
