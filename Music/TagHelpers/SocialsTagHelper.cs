﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Threading.Tasks;

public class SocialsTagHelper : TagHelper
{
    public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
    {
        output.TagName = "div";
        // получаем вложенный контекст из дочерних tag-хелперов
        var target = await output.GetChildContentAsync();
        var content = "<h3>Социальные сети</h3>" + target.GetContent();
        output.Content.SetHtmlContent(content);
    }
}

public class VkTagHelper : TagHelper
{
    private const string address = "https://vk.com/id171850614";
    public override void Process(TagHelperContext context, TagHelperOutput output)
    {

        output.TagName = "a";
        output.Attributes.SetAttribute("href", address);
        output.Content.SetContent("ВК");
    }
}

public class InstagramTagHelper : TagHelper
{
    private const string address = "https://www.instagram.com/alina.masetich";
    public override void Process(TagHelperContext context, TagHelperOutput output)
    {
        output.TagName = "a";
        output.Attributes.SetAttribute("href", address);
        output.Content.SetContent("Инстаграм");
    }
}