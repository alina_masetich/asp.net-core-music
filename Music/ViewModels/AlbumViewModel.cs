﻿using Music.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Music.ViewModels
{
    public class AlbumViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public List<Song> Songs { get; set; }
        public List<Comment> Comments { get; set; }
        public Album Album { get; set; }
    }
}
